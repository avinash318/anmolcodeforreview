package com.nt.model;

public class Deliveries {

	private int MATCH_ID;
	private int INNING;
	private String BATTING_TEAM;
	private String BOWLING_TEAM;
	private int OVER;
	private int BALL;
	private String BATSMAN;
	private String BOWLER;
	private int WIDE_RUNS;
	private int BYE_RUNS;
	private int LEGBYE_RUNS;
	private int NOBALL_RUNS;
	private int PENALTY_RUNS;
	private int BATSMAN_RUNS;
	private int EXTRA_RUNS;
	private int TOTAL_RUNS;
	public int getMATCH_ID() {
		return MATCH_ID;
	}
	public void setMATCH_ID(int mATCH_ID) {
		MATCH_ID = mATCH_ID;
	}
	public int getINNING() {
		return INNING;
	}
	public void setINNING(int iNNING) {
		INNING = iNNING;
	}
	public String getBATTING_TEAM() {
		return BATTING_TEAM;
	}
	public void setBATTING_TEAM(String bATTING_TEAM) {
		BATTING_TEAM = bATTING_TEAM;
	}
	public String getBOWLING_TEAM() {
		return BOWLING_TEAM;
	}
	public void setBOWLING_TEAM(String bOWLING_TEAM) {
		BOWLING_TEAM = bOWLING_TEAM;
	}
	public int getOVER() {
		return OVER;
	}
	public void setOVER(int oVER) {
		OVER = oVER;
	}
	public int getBALL() {
		return BALL;
	}
	public void setBALL(int bALL) {
		BALL = bALL;
	}
	public String getBATSMAN() {
		return BATSMAN;
	}
	public void setBATSMAN(String bATSMAN) {
		BATSMAN = bATSMAN;
	}
	public String getBOWLER() {
		return BOWLER;
	}
	public void setBOWLER(String bOWLER) {
		BOWLER = bOWLER;
	}
	public int getWIDE_RUNS() {
		return WIDE_RUNS;
	}
	public void setWIDE_RUNS(int wIDE_RUNS) {
		WIDE_RUNS = wIDE_RUNS;
	}
	public int getBYE_RUNS() {
		return BYE_RUNS;
	}
	public void setBYE_RUNS(int bYE_RUNS) {
		BYE_RUNS = bYE_RUNS;
	}
	public int getLEGBYE_RUNS() {
		return LEGBYE_RUNS;
	}
	public void setLEGBYE_RUNS(int lEGBYE_RUNS) {
		LEGBYE_RUNS = lEGBYE_RUNS;
	}
	public int getNOBALL_RUNS() {
		return NOBALL_RUNS;
	}
	public void setNOBALL_RUNS(int nOBALL_RUNS) {
		NOBALL_RUNS = nOBALL_RUNS;
	}
	public int getPENALTY_RUNS() {
		return PENALTY_RUNS;
	}
	public void setPENALTY_RUNS(int pENALTY_RUNS) {
		PENALTY_RUNS = pENALTY_RUNS;
	}
	public int getBATSMAN_RUNS() {
		return BATSMAN_RUNS;
	}
	public void setBATSMAN_RUNS(int bATSMAN_RUNS) {
		BATSMAN_RUNS = bATSMAN_RUNS;
	}
	public int getEXTRA_RUNS() {
		return EXTRA_RUNS;
	}
	public void setEXTRA_RUNS(int eXTRA_RUNS) {
		EXTRA_RUNS = eXTRA_RUNS;
	}
	public int getTOTAL_RUNS() {
		return TOTAL_RUNS;
	}
	public void setTOTAL_RUNS(int tOTAL_RUNS) {
		TOTAL_RUNS = tOTAL_RUNS;
	}

	}
