package com.nt.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import com.nt.model.Deliveries;

@Repository
public class ExcelDao {

	private static final String query = "select * from deliveries where rownum<=10000";
	@Autowired
	private JdbcTemplate template;

	public List<Deliveries> getExcel() {

		List<Deliveries> listdeliveries = new ArrayList<>();

		List<Deliveries> deliverieslist = template.query(query, new ResultSetExtractor<List<Deliveries>>() {

			@Override
			public List<Deliveries> extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					Deliveries deliveries = new Deliveries();
					deliveries.setMATCH_ID(rs.getInt("MATCH_ID"));
					deliveries.setINNING(rs.getInt("INNING"));
					deliveries.setBATTING_TEAM(rs.getString("BATTING_TEAM"));
					deliveries.setBOWLING_TEAM(rs.getString("BOWLING_TEAM"));
					deliveries.setOVER(rs.getInt("OVER"));
					deliveries.setBALL(rs.getInt("BALL"));
					deliveries.setBATSMAN(rs.getString("BATSMAN"));
					deliveries.setBOWLER(rs.getString("BOWLER"));
					deliveries.setWIDE_RUNS(rs.getInt("WIDE_RUNS"));
					deliveries.setBYE_RUNS(rs.getInt("BYE_RUNS"));
					deliveries.setLEGBYE_RUNS(rs.getInt("LEGBYE_RUNS"));
					deliveries.setNOBALL_RUNS(rs.getInt("NOBALL_RUNS"));
					deliveries.setPENALTY_RUNS(rs.getInt("PENALTY_RUNS"));
					deliveries.setBATSMAN_RUNS(rs.getInt("BATSMAN_RUNS"));
					deliveries.setEXTRA_RUNS(rs.getInt("EXTRA_RUNS"));
					deliveries.setTOTAL_RUNS(rs.getInt("TOTAL_RUNS"));
					listdeliveries.add(deliveries);
				}
				return listdeliveries;
			}
		});

		return deliverieslist;
	}
}
