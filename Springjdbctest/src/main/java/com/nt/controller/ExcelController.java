package com.nt.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nt.dao.ExcelDao;
import com.nt.model.Deliveries;

import oracle.net.aso.r;

@RestController
@ComponentScan("com.nt.dao")
public class ExcelController {
  
	@Autowired
	private ExcelDao dao;
	
	@RequestMapping(value="/getexcel.htm",produces= {"application/xlsx"})
	public String getData(HttpServletResponse req) throws IOException {
		List<Deliveries> listdata = dao.getExcel();
		XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Datatypes in Java");
         int rowNum = 0;
       for (Deliveries value : listdata) {
            Row row = sheet.createRow(rowNum++);
           row.createCell(0).setCellValue(value.getBATSMAN());
           row.createCell(1).setCellValue(value.getBATSMAN());
           row.createCell(2).setCellValue(value.getBATSMAN());
           row.createCell(3).setCellValue(value.getBATSMAN());
           row.createCell(4).setCellValue(value.getBATSMAN());
           row.createCell(5).setCellValue(value.getBATSMAN());
           row.createCell(6).setCellValue(value.getBATSMAN());
           row.createCell(7).setCellValue(value.getBATSMAN());
           row.createCell(8).setCellValue(value.getBATSMAN());
           row.createCell(9).setCellValue(value.getBATSMAN());
           row.createCell(10).setCellValue(value.getBATSMAN());
           row.createCell(11).setCellValue(value.getBATSMAN());
           row.createCell(12).setCellValue(value.getBATSMAN());
           row.createCell(13).setCellValue(value.getBATSMAN());
           row.createCell(14).setCellValue(value.getBATSMAN());
           row.createCell(15).setCellValue(value.getBATSMAN());
           row.createCell(16).setCellValue(value.getBATSMAN());
           row.createCell(17).setCellValue(value.getBATSMAN());
           row.createCell(18).setCellValue(value.getBATSMAN());
           row.createCell(19).setCellValue(value.getBATSMAN());
           row.createCell(20).setCellValue(value.getBATSMAN());
               
        }
BufferedOutputStream stream=new BufferedOutputStream(req.getOutputStream());
req.setHeader("Content-disposition", "attachment;filename=something.xlsx");

workbook.write(stream);
        
 workbook.close();      
  
  return null;
	
}
}
